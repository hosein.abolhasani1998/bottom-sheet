import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import AppWithCallbackAfterRender from './AppWithCallbackAfterRender';
import ReactGA from 'react-ga';

ReactGA.initialize('G-MGQBL0LR3F');

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <AppWithCallbackAfterRender />
  </React.StrictMode>
);
