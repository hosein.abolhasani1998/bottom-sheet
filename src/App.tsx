import { useState } from 'react';
// Common components
import BottomSheet from './components/BottomSheet';
// Assets
import viteLogo from '/vite.svg';
// Styles
import './App.css';
import ReactGA from 'react-ga';

function App() {
  const [open, setOpen] = useState(false);

  function openSheet() {
    ReactGA.event({
      label: 'User Interactions',
      category: 'Actions',
      action: 'Open Bottom Sheet',
      value: 25,
    });
    setOpen(true);
  }
  function closeSheet() {
    setOpen(false);
  }

  return (
    <>
      <div>
        <img src={viteLogo} className="logo" alt="Vite logo" />
      </div>
      <div className="card">
        <button className="btn" onClick={openSheet}>
          open sheet
        </button>
        <p>
          This <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <BottomSheet visible={open} onDismiss={closeSheet} />
    </>
  );
}

export default App;
