import { FC } from 'react';
// UI frameworks
import { Star } from 'lucide-react';
// Styles
import styles from './index.module.css';

interface Props {
  username: string;
  rate: number;
  distance: number;
  message: string;
  className?: string;
}

const Comment: FC<Props> = (props) => {
  return (
    <div className={`${styles['box']} ${props.className || ''}`}>
      <p className={styles['username']}>{props.username}</p>

      <div className={styles['footer']}>
        <span className={styles['row']}>
          {props.rate} <Star color="#5c4c4a" width={16} /> {'   '}
        </span>
        &#x2022;
        <span className={styles['row']}>
          {props.message} &#x2022; {props.distance} mi
        </span>
      </div>
    </div>
  );
};

export default Comment;
