import { FC } from 'react';
// Styles
import styles from './index.module.css';

interface Props {
  avatarSrc: string;
  name: string;
  reviewCount: number;
  subTitle: string;
}
const UserProfile: FC<Props> = (props) => {
  return (
    <div className={styles['box']}>
      <div className={styles['row']}>
        <img
          className={styles['avatar']}
          src={props.avatarSrc}
          alt={props.name}
        />
        <div className={styles['column']}>
          <p className={styles['username']}>{props.name}</p>
          <span className={styles['subtitle']}>
            {props.subTitle} &#x2022; {props.reviewCount} reviews
          </span>
        </div>
      </div>
      <button className={styles['follow-btn']}>Follow</button>
    </div>
  );
};

export default UserProfile;
