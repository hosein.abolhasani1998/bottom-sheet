import { ChangeEvent, FC, useMemo } from 'react';
// Styles
import styles from './index.module.css';

interface Props {
  searchText: string;
  onChange: (searchTerm: string) => void;
  className?: string;
  visible?: boolean;
}

const SearchBar: FC<Props> = (props) => {
  function handleChangeText(event: ChangeEvent<HTMLInputElement>) {
    props.onChange(event.target.value);
  }

  const paperStyles = useMemo(() => {
    const visible = props.visible;
    const styles = {
      opacity: !visible ? 0 : 1,
      height: !visible ? 0 : 'auto',
      margin: !visible ? 0 : '16px',
      padding: !visible ? 0 : '12px 8px',
    };
    return styles;
  }, [props.visible]);

  return (
    <div
      className={`${styles['box']} ${props.className || ''}`}
      style={paperStyles}
    >
      <img src="/map.png" alt="google map" className={styles['icon']} />
      <input
        className={styles['input']}
        value={props.searchText}
        onChange={handleChangeText}
        placeholder="Search here"
      />
      <img src="/me.jpeg" alt="me" className={styles['icon']} />
    </div>
  );
};

SearchBar.defaultProps = {
  visible: true,
};

export default SearchBar;
