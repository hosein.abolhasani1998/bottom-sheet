import { FC } from 'react';
// UI frameworks
import { Heart, MoreHorizontal } from 'lucide-react';
// Styles
import styles from './index.module.css';

interface Props {
  time: string;
  poster: string;
  className?: string;
}

const Post: FC<Props> = (props) => {
  return (
    <div className={`${styles['box']} ${props.className || ''}`}>
      <img src={props.poster} alt="" className={styles['poster']} />
      <div className={styles['footer']}>
        <span className={styles['time']}>{props.time}</span>
        <div className={styles['action-box']}>
          <Heart color="#5C4C4A" />
          <MoreHorizontal color="#5C4C4A" />
        </div>
      </div>
    </div>
  );
};

export default Post;
