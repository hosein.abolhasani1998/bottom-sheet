import type { CSSProperties } from 'react';

export const initialSnap = 2;
export const snapPoints = [-1, 400, 200];

export const baseContainerStyles: CSSProperties = {
  backgroundColor: '#F6F3F1',
  borderTopRightRadius: 25,
  borderTopLeftRadius: 25,
  boxShadow: 'none',
};
export const containerStylesFullHeight: CSSProperties = {
  backgroundColor: '#F6F3F1',
  borderTopRightRadius: 0,
  borderTopLeftRadius: 0,
  boxShadow: 'none',
  transform: 'none',
  minHeight: '100%',
};
