import { FC, useState } from 'react';
//UI frameworks
import Sheet from 'react-modal-sheet';
// Common components
import UserProfile from '../UserProfile';
import Comment from '../Comment';
import Post from '../Post';
import SearchBar from '../SearchBar';
// Utilities
import * as utils from './index.utils';
// Styles
import styles from './index.module.css';

interface Props {
  visible: boolean;
  onDismiss: () => void;
}

const BottomSheet: FC<Props> = (props) => {
  const [snapIndex, setSnapIndex] = useState(utils.initialSnap);
  const [search, setSearch] = useState('');

  const fullHeight = snapIndex === 0;

  return (
    <Sheet
      isOpen={props.visible}
      onClose={props.onDismiss}
      snapPoints={utils.snapPoints}
      initialSnap={utils.initialSnap}
      onSnap={setSnapIndex}
      detent="full-height"
    >
      <Sheet.Container
        style={
          fullHeight
            ? utils.containerStylesFullHeight
            : utils.baseContainerStyles
        }
      >
        <SearchBar
          visible={fullHeight}
          searchText={search}
          onChange={setSearch}
        />
        {!fullHeight ? <Sheet.Header /> : null}
        <Sheet.Content>
          <div className={styles['container']}>
            <h4 className={styles['title']}>Latest in the area</h4>
            <UserProfile
              name="Seyyed Ali"
              subTitle="Local Guide"
              avatarSrc="/avatar.jpeg"
              reviewCount={14}
            />
            <Post
              className={styles['post']}
              time="2 days ago"
              poster="/poster.jpg"
            />
            <Comment
              username={'Mud Island Amphitheater'}
              rate={4.4}
              distance={0.6}
              message={'Outdoor seasonal theater'}
            />
          </div>
        </Sheet.Content>
      </Sheet.Container>
      <Sheet.Backdrop />
    </Sheet>
  );
};

export default BottomSheet;
