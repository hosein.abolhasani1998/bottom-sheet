/* eslint-disable @typescript-eslint/no-floating-promises */
import { useLayoutEffect } from 'react';
import App from './App';
import ReactGA from 'react-ga';
import { registerSW } from 'virtual:pwa-register';

if ('serviceWorker' in navigator) {
  registerSW();
}

export default function AppWithCallbackAfterRender() {
  useLayoutEffect(() => {
    console.log('app rendered');
    ReactGA.pageview(window.location.pathname);
  }, []);

  return <App />;
}
