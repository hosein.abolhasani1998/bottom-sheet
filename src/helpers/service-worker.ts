/* eslint-disable no-async-promise-executor */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-misused-promises */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import packageJSON from '../../package.json';
import ReactGa from 'react-ga';

export function getVersion(): string {
  return packageJSON.version;
}

export const isServiceWorker = (): boolean => 'serviceWorker' in navigator;

export async function getRegisteredServiceWorker(): Promise<
  ServiceWorkerRegistration | undefined
> {
  return await navigator.serviceWorker.getRegistration('/');
}

export const forceCheckNewContentAvailable = (): Promise<boolean> => {
  return new Promise<boolean>(async (resolve, reject) => {
    if (isServiceWorker()) {
      const OfflinePluginRuntime = await import(
        /* webpackChunkName: "lib-OfflinePluginRuntime" */ 'offline-plugin/runtime'
      );

      try {
        await OfflinePluginRuntime.update();

        resolve(true);
      } catch (e) {
        reject(false);
      }
    } else {
      reject(false);
    }
  });
};

export function registerServiceWorker() {
  // Service worker utility
  import(
    /* webpackChunkName: "lib-OfflinePluginRuntime" */ 'offline-plugin/runtime'
  ).then((OfflinePluginRuntime) => {
    console.log('registered');
    /**
     * Browser fetches ServiceWorker file each time user navigates to your website. If new ServiceWorker is found, browser immediately runs it along side with current SW. This new SW gets only install event at this time, which allows it to prepare/cache assets of the new version. New SW doesn't start controlling pages until all tabs of your website are closed, this is by design in the ServiceWorker.
     * AppCache has slightly simpler update mechanism: browser also downloads manifest.appcache on each navigation to your site (simplified) and if new AppCache is available, browser installs new AppCache and removes old one. This means that on a next page refresh browser will load files from the new AppCache.
     * Sometimes, AppCache is the root of confusion--one may just expect SW to have the same update process as AppCache have. Another confusing thing could be Cmd/Ctrl+R combination. One may think that it forces browser to update and activate SW. This is wrong, Cmd/Ctrl+R doesn't updates or reloads into a new SW, it just tells to browser to bypass currently controlling ServiceWorker.
     * If you have only 1 tab of your website opened, then this flow can update SW:
     *  1.Refresh the page (browser downloads new SW)
     *  2. Ctrl/Cmd+Refresh the page (browser bypasses SW). Now current SW doesn't control any pages, so it will be discarded and new SW will be activated
     *  3. Refresh the page (new SW is now in place and controls the page)
     * This is how it works. Of course, there is a way to update SW/AppCache when you want it, you are the developer after all :-)
     */
    if ('serviceWorker' in navigator) {
      OfflinePluginRuntime.install({
        // Event called exactly once when ServiceWorker or AppCache is installed. Can be useful to display "App is ready for offline usage" message.
        onInstalled() {
          console.log('[SW] Event:', 'Application installed successfully');

          ReactGa.event({
            category: 'pwa',
            action: 'cached',
          });
        },
        // Event called when update is found and browsers started updating process. At this moment, some assets are downloading.
        onUpdating() {
          console.log('[SW] Event:', 'Application is updating');
        },
        // Tells to new SW to take control immediately
        // Event called when onUpdating phase finished. All required assets are downloaded at this moment and are ready to be updated. Call runtime.applyUpdate() to apply update.
        onUpdateReady() {
          console.log(
            '[SW] Event:',
            'There is new version of the Application and ready to install.',
            `Your version: ${getVersion()}`
          );

          ReactGa.event({
            category: 'pwa',
            action: 'update-ready',
            label: `current-version: ${getVersion()}`,
          });

          const newContentAvailable = new Event('newContentAvailable');

          window.dispatchEvent(newContentAvailable);
        },
        // Reload the web page to load into the new version
        // Event called when update is applied, either by calling runtime.applyUpdate() or some other way by a browser itself.
        onUpdated() {
          console.log(
            '[SW] Event:',
            'New update installed successfully. Enjoy it!',
            `Your version: ${getVersion()}`
          );

          ReactGa.event({
            category: 'pwa',
            action: 'updated',
            label: `new-version: ${getVersion()}`,
          });

          window.location.reload();
        },
        // Event called when onUpdating phase failed by some reason. Nothing is downloaded at this moment and current update process in your code should be canceled or ignored.
        onUpdateFailed: () => {
          console.log(
            '[SW] Event:',
            'Updating the Application has failed. Try in the next view',
            `Your version: ${getVersion()}`
          );

          ReactGa.event({
            category: 'pwa',
            action: 'failed-to-update',
            label: `current-version: ${getVersion()}`,
          });
        },
      });
    }
  }); // PWA configuration
}

export async function unregisterServiceWorker() {
  if (isServiceWorker()) {
    try {
      // Removing Service Workers Programmatically
      const registrations = await navigator.serviceWorker.getRegistrations();

      for (const registration of registrations) {
        await registration.unregister();
      }

      ReactGa.event({
        category: 'pwa',
        action: 'hard-clear-cache',
        label: 'successful',
      });

      registerServiceWorker();
    } catch (err) {
      ReactGa.event({
        category: 'pwa',
        action: 'hard-clear-cache',
        label: 'failed',
      });
      console.log('[SW] Unregister error:', err);
    }
  }
}
